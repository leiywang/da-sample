package leiwang.da.sample;


import leiwang.da.sample.test.model.*;
import leiwang.da.sample.test.model.composite.ClassRoom;
import leiwang.da.sample.test.model.composite.Roster;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class DAFormatTest {
    private DAFormat daFormat;

    @Before
    public void setUp() {
        daFormat = new DAFormat();
    }

    @Test
    public void testPrimitive() {
        Assert.assertEquals("1" + System.lineSeparator(), daFormat.format(1));
        Assert.assertEquals("1" + System.lineSeparator(), daFormat.format(new Integer(1)));
        Assert.assertEquals("a" + System.lineSeparator(), daFormat.format("a"));
        Assert.assertEquals("null" + System.lineSeparator(), daFormat.format(null));
        Assert.assertEquals("true" + System.lineSeparator(), daFormat.format(true));
        Assert.assertEquals("true" + System.lineSeparator(), daFormat.format(Boolean.TRUE));
        Assert.assertEquals("FEMALE" + System.lineSeparator(), daFormat.format(Gender.FEMALE));
        Assert.assertEquals("1.1" + System.lineSeparator(), daFormat.format(1.1f));
        Assert.assertEquals("1.1" + System.lineSeparator(), daFormat.format(new Float(1.1f)));
        Assert.assertEquals("1.123" + System.lineSeparator(), daFormat.format(1.123d));
        Assert.assertEquals("1.123" + System.lineSeparator(), daFormat.format(1.123d));
        Assert.assertEquals("c" + System.lineSeparator(), daFormat.format('c'));
        Assert.assertEquals("c" + System.lineSeparator(), daFormat.format(new Character('c')));
        Assert.assertEquals("1" + System.lineSeparator(), daFormat.format(1L));
        Assert.assertEquals("1" + System.lineSeparator(), daFormat.format(new Long(1L)));
        Assert.assertEquals("123" + System.lineSeparator(), daFormat.format((byte) 123));
        Assert.assertEquals("123" + System.lineSeparator(), daFormat.format(new Byte((byte) 123)));
        Assert.assertEquals("1234" + System.lineSeparator(), daFormat.format((short) 1234));
        Assert.assertEquals("1234" + System.lineSeparator(), daFormat.format(new Short((short) 1234)));
    }

    @Test
    public void testSimpleObject() {
        String expected = this.loadExpectedFile("SimpleObject.txt");
        Assert.assertEquals(expected, daFormat.format(this.createPerson()));
    }

    @Test
    public void testInheritanceObject() {
        String expected = this.loadExpectedFile("InheritanceObject.txt");
        Assert.assertEquals(expected, daFormat.format(this.createStudent()));
    }

    @Test
    public void testPrimitiveCollection() {
        List<String> aList = new ArrayList<>();
        aList.add("abc");
        aList.add("abc");
        String expected = this.loadExpectedFile("PrimitiveCollection.txt");
        Assert.assertEquals(expected, daFormat.format(aList));
    }

    @Test
    public void testPrimitiveArray() {
        String[] anArray = new String[] {"abc", "xyz"};

        String expected = this.loadExpectedFile("PrimitiveArray.txt");
        Assert.assertEquals(expected, daFormat.format(anArray));
    }

    @Test
    public void testCollectionOfObject() {
        Collection<Student> students = new ArrayList<>();
        students.add(this.createStudent());
        students.add(this.createStudent());

        String expected = this.loadExpectedFile("CollectionOfObjects.txt");
        Assert.assertEquals(expected, daFormat.format(students));
    }

    @Test
    public void testArrayOfObject() {
        Student[] students = new Student[2];
        students[0] = this.createStudent();
        students[0] = this.createStudent();

        String expected = this.loadExpectedFile("ArrayOfObjects.txt");
        Assert.assertEquals(expected, daFormat.format(students));
    }

    @Test
    public void testObjectWithCollection() {
        String expected = this.loadExpectedFile("ObjectWithCollection.txt");
        Assert.assertEquals(expected, daFormat.format(this.createClassRoom()));
    }

    @Test
    public void testObjectWithArray() {
        String expected = this.loadExpectedFile("ObjectWithArray.txt");
        Assert.assertEquals(expected, daFormat.format(this.createRoster()));
    }

    @Test
    public void testCollectionOfArray() {
        Collection<Student[]> studentsCollection = new ArrayList<>();
        studentsCollection.add(new Student[] {this.createStudent(), this.createStudent()});
        studentsCollection.add(new Student[] {this.createStudent(), this.createStudent()});

        String expected = this.loadExpectedFile("CollectionOfArray.txt");
        Assert.assertEquals(expected, daFormat.format(studentsCollection));
    }

    @Test
    public void testCollectionOfCollection() {
        Collection<Collection<Student>> studentsCollection = new ArrayList<>();
        studentsCollection.add(Arrays.asList(this.createStudent(), this.createStudent()));
        studentsCollection.add(Arrays.asList(this.createStudent(), this.createStudent()));

        String expected = this.loadExpectedFile("CollectionOfCollection.txt");
        Assert.assertEquals(expected, daFormat.format(studentsCollection));
    }

    @Test
    public void testArrayOfCollection() {
        Collection<Student>[] studentsCollections = new Collection[2];
        studentsCollections[0] = Arrays.asList(this.createStudent(), this.createStudent());
        studentsCollections[1] = Arrays.asList(this.createStudent(), this.createStudent());

        String expected = this.loadExpectedFile("ArrayOfCollection.txt");
        Assert.assertEquals(expected, daFormat.format(studentsCollections));
    }

    @Test
    public void testArrayOfArray() {
        Student[][] studentsArray = new Student[2][2];
        studentsArray[0][0] = this.createStudent();
        studentsArray[0][1] = this.createStudent();
        studentsArray[1][0] = this.createStudent();
        studentsArray[1][1] = this.createStudent();

        String expected = this.loadExpectedFile("ArrayOfArray.txt");
        Assert.assertEquals(expected, daFormat.format(studentsArray));
    }

    @Test
    public void testObjectWithNullCollection() {
        ClassRoom classRoom = new ClassRoom();

        String expected = this.loadExpectedFile("ObjectWithNullCollection.txt");
        Assert.assertEquals(expected, daFormat.format(classRoom));

    }

    @Test
    public void testObjectWithNullArray() {
        Roster roster = new Roster();

        String expected = this.loadExpectedFile("ObjectWithNullArray.txt");
        Assert.assertEquals(expected, daFormat.format(roster));
    }

    private Person createPerson() {
        Name name = new Name();
        name.first = "A";
        name.last = "B";
        Person person = new Person();
        person.name = name;
        person.age = 17;
        return person;
    }

    private Student createStudent() {
        Name name = new Name();
        name.first = "A";
        name.last = "B";
        Student student = new Student();
        student.age = 10;
        student.school = "School A";
        return student;
    }

    private ClassRoom createClassRoom() {
        Student student1 = this.createStudent();
        Student student2 = this.createStudent();
        ClassRoom classRoom = new ClassRoom();
        classRoom.students = Arrays.asList(student1, student2);
        return classRoom;
    }

    private Roster createRoster() {
        Course course1 = new Course();
        course1.name = "Course 1";
        Course course2 = new Course();
        course2.name = "Course 2";

        Roster roster = new Roster();
        roster.courses = new Course[] {course1, course2};
        return roster;
    }

    private String loadExpectedFile(String path) {
        try {

            URI uri = getClass().getResource(path).toURI();
            return new String(Files.readAllBytes(Paths.get(uri)));
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }
}
