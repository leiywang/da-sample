package leiwang.da.sample;

import leiwang.da.sample.serializer.Serializer;

/**
 * An in memory String implementation of <code>DAFormatter</code>
 */
public class StringDAFormatWriter implements DAFormatWriter{
    private StringBuilder builder = new StringBuilder();
    private int indent;

    public void write(Object object, Serializer serializer) {
        indent = 0;
        builder = new StringBuilder();
        serializer.serialize(object, null, this);
    }

    public void indent() {
        indent++;
    }

    public void unindent() {
        indent--;
    }

    public void writeLine(String line) {
        this.beginLine();
        builder.append(line);
        this.endLine();
    }

    private void beginLine() {
        for(int i = 0; i < indent; i++) {
            builder.append("\t");
        }
    }

    private void endLine() {
        builder.append(System.lineSeparator());
    }

    public String getString() {
        return this.builder.toString();
    }
}
