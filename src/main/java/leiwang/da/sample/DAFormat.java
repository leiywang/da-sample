package leiwang.da.sample;

import leiwang.da.sample.serializer.*;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

/**
 * Entry point into DAFormat functionality. It offers a format method to format any object into DA Format.
 * In addition, it also contains runtime registration of serializers for different types. In the currennt implementation,
 * the serializers are static.
 */
public class DAFormat {
    private static final PrimitiveSerializer PRIMITIVE_SERIALIZER = new PrimitiveSerializer();

    /**
     * Format takes any object and outputs DA Format.
     * It can thrown <code>IllegalArgumentException</code> when an error is encountered.
     * @param object an Object to be formatted
     * @return DA-Formatted String
     */
    public String format(Object object) {
        DAFormatWriter writer = new StringDAFormatWriter();
        // due to type erasure, we assume it's Object type if a collection is passed in.
        Serializer serializer = this.getSerializer(object, Object.class);
        writer.write(object, serializer);
        return writer.getString();
    }

    /**
     * Find a <code>Serializer</code> for the <code>Class</code> type.
     * @param value Object value
     * @return <code>Serializer</code> for the <code>Class</code> type.
     */
    public Serializer getSerializer(Object value) {
        return this.getSerializer(value, null);
    }

    /**
     * Find a <code>Serializer</code> for the <code>Field</code> type
     * @param field type of <code>Field</code>
     * @param value value of <code>Field</code>
     * @return <code>Serializer</code> for the <code>Field</code>
     */
    public Serializer getSerializer(Field field, Object value) {
        if(field.getGenericType() instanceof ParameterizedType) {
            ParameterizedType pType = (ParameterizedType) field.getGenericType() ;
            Type[] arr = pType.getActualTypeArguments();

            return getSerializer(value, (Class<?>) arr[0]);
        } else {
            return getSerializer(value, null);
        }
    }

    private Serializer getSerializer(Object value, Class<?> subType) {
        if(value == null) return PRIMITIVE_SERIALIZER;
        Class<?> clazz = value.getClass();
        if (isPrimitiveOrPrimitiveWrapperOrString(clazz)) {
            return PRIMITIVE_SERIALIZER;
        } else if(isCollection(clazz)) {
            // Due to type erasure, we will assume all elements are type of collection
            return new CollectionSerializer(this, subType);
        } else if(this.isArray(clazz)) {
            return new ArraySerializer(this, clazz.getComponentType());
        } else {
            return new ObjectSerializer(this);
        }
    }

    private boolean isCollection(Class<?> clazz) {
        return Collection.class.isAssignableFrom(clazz);
    }

    private boolean isArray(Class<?> clazz) {
        return clazz.isArray();
    }

    private boolean isPrimitiveOrPrimitiveWrapperOrString(Class<?> type) {
        return (type.isPrimitive() && type != void.class) ||
                type == Short.class ||
                type == Double.class ||
                type == Float.class ||
                type == Character.class ||
                type == Byte.class ||
                type == String.class ||
                type == Long.class ||
                type == Integer.class ||
                type == Boolean.class ||
                type.isEnum(); // going to assume enum is also a primitive type (we can have an enum serializer in the future).
    }
}
