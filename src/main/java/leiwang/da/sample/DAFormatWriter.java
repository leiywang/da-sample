package leiwang.da.sample;

import leiwang.da.sample.serializer.Serializer;

/**
 * A <code>DAFormatWriter</code> is used to write DA Format. Currently, only implementation is
 * based on in memory string. In the future, we can other impplementation, e.g. File based.
 */
public interface DAFormatWriter {
    /**
     * Writes an object into DA Format
     * @param object an object to be written
     * @param serializer <code>Serializer</code> for the object.
     */
    void write(Object object, Serializer serializer) ;

    /**
     * Adds an indentation to existing indentation format.
     */
    void indent() ;

    /**
     * Removes an indentation from existing indentation format.
     */
    void unindent() ;

    /**
     * Writes a line using current indentation format.
     * @param line a line to be written.
     */
    void writeLine(String line);

    /**
     * Gets DA Formatted representation of what's currently in buffer.
     * @return DA formatted string.
     */
    String getString();
}
