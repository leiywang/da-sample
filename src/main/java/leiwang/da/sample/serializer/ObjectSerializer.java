package leiwang.da.sample.serializer;

import leiwang.da.sample.DAFormat;
import leiwang.da.sample.DAFormatWriter;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

/**
 * Serializes any object that's not composite nor primitve.
 */
public class ObjectSerializer extends ComplexSerializer {
    public ObjectSerializer(DAFormat daFormat){
        super(daFormat);
    }

    @Override
    protected String getObjectClass(Object object) {
        return object.getClass().getCanonicalName();
    }

    @Override
    protected void serializeElement(Object object, DAFormatWriter writer) {
        Class<?> clazz = object.getClass();
        Set<Field> fieldSet = new HashSet<>();
        Field[] declaredFields = clazz.getDeclaredFields();
        Field[] allOtherFields = clazz.getFields();
        try {
            for (Field field : declaredFields) {
                field.setAccessible(true);
                Object value = field.get(object);
                Serializer serializer = daFormat.getSerializer(field, value);
                serializer.serialize(value, field.getName(), writer);
                fieldSet.add(field);
            }
            for (Field field : allOtherFields) {
                if(!fieldSet.contains(field)) {
                    field.setAccessible(true);
                    Object value = field.get(object);
                    Serializer serializer = daFormat.getSerializer(field, value);
                    serializer.serialize(value, field.getName(), writer);
                }
            }

        }catch(IllegalAccessException e) {
            throw new IllegalArgumentException("Formatting error", e);
        }
    }
}
