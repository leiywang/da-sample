package leiwang.da.sample.serializer;

import leiwang.da.sample.DAFormat;

/**
 * Collection Serializer serializes <code>array</code>
 */
public class ArraySerializer extends CompositeSerializer {

    public ArraySerializer(DAFormat daFormat, Class<?> componentType) {
        super(daFormat, componentType);
    }

    @Override
    protected String getClassType() {
        return "Array[%s]";
    }

    @Override
    protected Object[] getArray(Object composite) {
        return (Object[]) composite;
    }
}
