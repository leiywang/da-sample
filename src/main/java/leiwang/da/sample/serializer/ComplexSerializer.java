package leiwang.da.sample.serializer;

import leiwang.da.sample.DAFormat;
import leiwang.da.sample.DAFormatWriter;

/**
 * Complex serializer serializes all objects that's not Primitive
 *
 */
public abstract class ComplexSerializer implements Serializer{
    protected final DAFormat daFormat;

    protected ComplexSerializer(DAFormat daFormat) {
        this.daFormat = daFormat;
    }

    public void serialize(Object object, String fieldName, DAFormatWriter writer) {
        boolean indent = false;
        if(fieldName != null) {
            if(fieldName.length() > 0)
                writer.writeLine(fieldName + ":");
            writer.indent();
            indent = true;
        }
        writer.writeLine("------------------------------------------");
        writer.writeLine("Object of class: " + this.getObjectClass(object));
        writer.writeLine("------------------------------------------");

        this.serializeElement(object, writer);

        if(indent)
            writer.unindent();
    }

    /**
     * Returns the name of the class of the object being serialized.
     * @param object Object to be serialized.
     * @return the name of the class of the object.
     */
    protected abstract String getObjectClass(Object object);

    /**
     * Serializes each element or field in this complex object.
     * @param object Object to be serialized.
     * @param writer the writer to which it will be serialized.
     */
    protected abstract void serializeElement(Object object, DAFormatWriter writer);
}
