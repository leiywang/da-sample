package leiwang.da.sample.serializer;

import leiwang.da.sample.DAFormatWriter;

/**
 * A <code>Serializer</code> serializes a type of an Object into DA Format.
 */
public interface Serializer {
    /**
     * Given a writer, it serializes an object and its field name.
     * @param object an Object to be serialized.
     * @param fieldName the name of field that represents this object.
     * @param writer a writer, where this object will be written to.
     */
    void serialize(Object object, String fieldName, DAFormatWriter writer);
}
