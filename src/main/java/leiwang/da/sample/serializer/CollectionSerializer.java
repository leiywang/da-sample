package leiwang.da.sample.serializer;

import leiwang.da.sample.DAFormat;

import java.util.Collection;

/**
 * Collection Serializer serializes <code>Collection</code>
 */
public class CollectionSerializer extends CompositeSerializer {

    public CollectionSerializer(DAFormat daFormat, Class<?> genericType) {
        super(daFormat, genericType);
    }

    @Override
    protected String getClassType() {
        return "Collection[%s]";
    }

    @Override
    protected Object[] getArray(Object object) {
        return ((Collection) object).toArray();
    }

}
