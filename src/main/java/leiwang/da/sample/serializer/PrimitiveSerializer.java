package leiwang.da.sample.serializer;

import leiwang.da.sample.DAFormatWriter;

/**
 * A <code>PrimitivieSerializer</code> serializes the following Object types or java primitive:
 * <ul>
 *     String
 *     Boolean/boolean
 *     Integer/int
 *     Short/short
 *     Double/double
 *     Float/float
 *     Character/char
 *     Byte/byte
 *     Long/long
 *     Enum
 *     null
 * </ul>
 */
public class PrimitiveSerializer implements Serializer{
    private static final String NULL = "null";
    @Override
    public void serialize(Object object, String fieldName, DAFormatWriter writer) {
        String line = fieldName != null && !fieldName.isEmpty() ? fieldName + ": " : "";
        if(object == null) {
            line = line + NULL;
        } else {
            line = line + object.toString();
        }
        writer.writeLine(line);
    }
}
